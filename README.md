# Link Controller

Python Sanic server

## After clone
```bash
python3.7 -m venv .venv
source .venv/bin/activate
pip install poetry
poetry update
export REDIS_CONNECTION=
```

## Develop

### Start Sanic server
```bash
python -m sanic autoapp.app --host=0.0.0.0 --port=8000
```

### Sort imports
```bash
poetry run isort -y
```

### Run linters
```bash
poetry run prospector
```

### Run tests
```bash
pytest tests/test.py
```

## Deploy
```bash
docker-compose build
docker-compose up
```