import re
from typing import List, Set

import tldextract
from app.extensions import conn

re_ref_domain = re.compile(r'^\d*\:')


def get_domain(link: str):
    domain = tldextract.extract(link)
    if domain:
        return domain.registered_domain
    return None


def validator_domains(domains: List):
    return [get_domain(domain) for domain in domains if get_domain(domain)]


async def create_domains(domains: List, timestamp: int):
    await conn.zadd(validator_domains(domains), timestamp)


async def get_list_domains(datetime_start: int, datetime_end: int) -> Set:
    domains = await conn.zrevrange_by_lex(datetime_start, datetime_end)
    return {re_ref_domain.sub('', domain.decode()) for domain in domains}
